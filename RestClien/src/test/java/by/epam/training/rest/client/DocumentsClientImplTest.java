package by.epam.training.rest.client;

import by.epam.training.rest.model.Chapter;
import by.epam.training.rest.model.Document;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DocumentsClientImplTest {

    @Test
    public void testDoGetWhenObjectWasNotCreated() {
        Assert.assertEquals("Should return null!", null, new DocumentsClientImpl().doGet(0));
    }

    @Test
    public void testDoGetWhenObjectWasCreated() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(11);
        document.setName("test Doc");
        document.setChapters(new ArrayList<>());
        client.doPost(document);
        Assert.assertEquals("Should return the same object!", document, client.doGet(11));
    }

    @Test
    public void testDoPost() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(123);
        document.setName("new Document");
        document.setChapters(new ArrayList<>());
        Assert.assertEquals("Should return the same object!", document, client.doPost(document));
        Assert.assertEquals("Service should contains the same object!", document, client.doGet(123));
    }

    @Test
    public void testDoPutWhenObjectWasCreated() {
        DocumentsClient client = new DocumentsClientImpl();
        client.doDelete(99);
        Document document = new Document();
        document.setId(99);
        document.setName("new Document");
        document.setChapters(new ArrayList<>());
        Assert.assertEquals("Should return the same object!", document, client.doPut(99, document));
        Assert.assertEquals("Service should contains the same object!", document, client.doGet(99));
    }

    @Test
    public void testDoPutWhenObjectWasUpdated() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(2);
        document.setName("document");
        document.setChapters(new ArrayList<>());
        client.doPost(document);
        document.setName("doc2");
        Assert.assertEquals("Should return null!", null, client.doPut(2, document));
        Assert.assertEquals("Should return document with new name!", document, client.doGet(2));
    }

    @Test
    public void testDoPutWhenObjectWasNotModify() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(3);
        document.setName("document");
        document.setChapters(new ArrayList<>());
        client.doPost(document);
        Assert.assertEquals("Should return null!", null, client.doPut(3, document));
    }

    @Test
    public void testDoPutChapterWhenChapterWasNotFound() {
        DocumentsClient client = new DocumentsClientImpl();
        client.doDelete(4);
        Document document = new Document();
        document.setId(4);
        document.setName("document");
        Chapter chapter = new Chapter();
        chapter.setId(1);
        chapter.setNumberOfPages(12);
        chapter.setChapterNumber(12);
        List<Chapter> chapters = new ArrayList<>();
        chapters.add(chapter);
        document.setChapters(chapters);
        client.doPost(document);

        Chapter newChapter = new Chapter();
        newChapter.setId(2);
        newChapter.setChapterNumber(10);
        newChapter.setNumberOfPages(10);

        document.getChapters().add(newChapter);
        Document responseDocument = client.doPut(4, 2, newChapter);
        Assert.assertEquals("Should return document with new chapter!", document, responseDocument);
    }

    @Test
    public void testDoPutChapterWhenChapterWasFound() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(5);
        document.setName("document");
        Chapter chapter = new Chapter();
        chapter.setId(1);
        chapter.setNumberOfPages(12);
        chapter.setChapterNumber(12);
        List<Chapter> chapters = new ArrayList<>();
        chapters.add(chapter);
        document.setChapters(chapters);
        client.doPost(document);

        chapter.setNumberOfPages(10);
        chapter.setChapterNumber(10);
        Document responseDocument = client.doPut(5, 1, chapter);
        Assert.assertEquals("Should return null!", null, responseDocument);
        Assert.assertEquals("Service should contains new version of document!", document, client.doGet(5));
    }

    @Test
    public void testDoDeleteDocument() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(6);
        document.setName("document");
        document.setChapters(new ArrayList<>());
        client.doPost(document);
        client.doDelete(6);
        Assert.assertEquals("Should return null!", null, client.doGet(6));
    }

    @Test
    public void testDoDeleteChapter() {
        DocumentsClient client = new DocumentsClientImpl();
        Document document = new Document();
        document.setId(6);
        document.setName("document");
        Chapter chapter = new Chapter();
        chapter.setId(1);
        chapter.setNumberOfPages(12);
        chapter.setChapterNumber(12);
        List<Chapter> chapters = new ArrayList<>();
        chapters.add(chapter);
        document.setChapters(chapters);
        client.doPost(document);
        client.doDelete(6, 1);
        Assert.assertEquals("List of chapters should be empty!",
                0, client.doGet(6).getChapters().size());
    }

}
