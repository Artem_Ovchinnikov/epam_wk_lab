package by.epam.training.rest.client;

import by.epam.training.rest.model.Chapter;
import by.epam.training.rest.model.Document;

public interface DocumentsClient {

    /**
     * Makes a GET request to REST service.
     *
     * @param documentId document ID for search.
     * @return object, if he was found, or null.
     */
    public Document doGet(long documentId);

    /**
     * Makes a POST request to REST service.
     *
     * @param document document for storing.
     * @return object, that was saved in service.
     */
    public Document doPost(Document document);

    /**
     * Makes a PUT request to REST service.
     *
     * @param documentId  document ID for search.
     * @param newDocument new version of document for storing.
     * @return object, if he was created, or null.
     */
    public Document doPut(long documentId, Document newDocument);

    /**
     * Makes a PUT request to REST service.
     *
     * @param documentId document ID for search.
     * @param chapterId  chapter ID in document.
     * @param newChapter new version of chapter for storing.
     * @return object, if chapter was created, or null.
     */
    public Document doPut(long documentId, long chapterId, Chapter newChapter);

    /**
     * Make a DELETE request to REST service.
     *
     * @param documentId document ID for search.
     * @param chapterId  chapter ID to delete.
     */
    public void doDelete(long documentId, long chapterId);

    /**
     * Make a DELETE request to REST service.
     *
     * @param documentId document ID to delete.
     */
    public void doDelete(long documentId);

}
