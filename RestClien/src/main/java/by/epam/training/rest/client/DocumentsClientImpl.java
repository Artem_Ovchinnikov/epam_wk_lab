package by.epam.training.rest.client;

import by.epam.training.rest.model.Chapter;
import by.epam.training.rest.model.Document;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class DocumentsClientImpl implements DocumentsClient {

    private static final Logger LOGGER = LogManager.getLogger(DocumentsClientImpl.class);
    private static final String SERVICE_URL = "http://localhost:8080/soap-rest-services/rest";
    private final HttpClient client;
    private static final String CONTENT_XML = "application/xml";

    private static final int HTTP_CODE_CREATED = 201;
    private static final int HTTP_CODE_NOT_FOUND = 404;

    public DocumentsClientImpl() {
        client = HttpClientBuilder.create().build();
    }

    @Override
    public Document doGet(long documentId) {
        String requestUrl = SERVICE_URL + "/documents/" + documentId;
        HttpGet request = new HttpGet(requestUrl);
        try {
            LOGGER.info("Do GET request : " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == HTTP_CODE_NOT_FOUND) {
                return null;
            }
            String json = readFromStream(response.getEntity().getContent());
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, Document.class);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public Document doPost(Document document) {
        String requestUrl = SERVICE_URL + "/documents/";
        HttpPost request = new HttpPost(requestUrl);
        StringEntity requestData = new StringEntity(convertInXml(document), StandardCharsets.UTF_8);
        requestData.setContentType(CONTENT_XML);
        request.setEntity(requestData);
        try {
            LOGGER.info("Do POST request : " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
            String json = readFromStream(response.getEntity().getContent());
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, Document.class);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public Document doPut(long documentId, Document newDocument) {
        String requestUrl = SERVICE_URL + "/documents/" + documentId;
        HttpPut request = new HttpPut(requestUrl);
        StringEntity requestData = new StringEntity(convertInXml(newDocument), StandardCharsets.UTF_8);
        requestData.setContentType(CONTENT_XML);
        request.setEntity(requestData);
        try {
            LOGGER.info("Do PUT request : " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == HTTP_CODE_CREATED) {
                String json = readFromStream(response.getEntity().getContent());
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(json, Document.class);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public Document doPut(long documentId, long chapterId, Chapter newChapter) {
        String requestUrl = SERVICE_URL + "/documents/" + documentId + "/chapters/" + chapterId;
        HttpPut request = new HttpPut(requestUrl);
        StringEntity requestData = new StringEntity(convertInXml(newChapter), StandardCharsets.UTF_8);
        requestData.setContentType(CONTENT_XML);
        request.setEntity(requestData);
        try {
            LOGGER.info("Do PUT request : " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() == HTTP_CODE_CREATED) {
                String json = readFromStream(response.getEntity().getContent());
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(json, Document.class);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return null;
    }

    @Override
    public void doDelete(long documentId, long chapterId) {
        String requestUrl = SERVICE_URL + "/documents/" + documentId + "/chapters/" + chapterId;
        HttpDelete request = new HttpDelete(requestUrl);
        try {
            LOGGER.info("Do DELETE request " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public void doDelete(long documentId) {
        String requestUrl = SERVICE_URL + "/documents/" + documentId;
        HttpDelete request = new HttpDelete(requestUrl);
        try {
            LOGGER.info("Do DELETE request " + requestUrl);
            HttpResponse response = client.execute(request);
            LOGGER.info("HTTP code : " + response.getStatusLine().getStatusCode());
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    private static String readFromStream(final InputStream stream) {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return builder.toString();
    }

    private static <T> String convertInXml(T object) {
        StringWriter xmlString = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(object, xmlString);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return xmlString.toString();
    }

}
