package by.epam.training.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Chapter model.
 */
@XmlRootElement(name = "chapter")
public class Chapter {

    private long id;
    private int chapterNumber;
    private int numberOfPages;

    public Chapter() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(int chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chapter chapter = (Chapter) o;

        if (id != chapter.id) return false;
        if (chapterNumber != chapter.chapterNumber) return false;
        return numberOfPages == chapter.numberOfPages;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + chapterNumber;
        result = 31 * result + numberOfPages;
        return result;
    }
}
