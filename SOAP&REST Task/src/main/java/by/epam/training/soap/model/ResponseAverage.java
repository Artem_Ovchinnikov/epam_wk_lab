package by.epam.training.soap.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * ResponseAverage model.
 */
@XmlRootElement(name="GetAverageResponse")
public class ResponseAverage {

    private double average;

    public ResponseAverage() {
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
