package by.epam.training.soap.service;

import by.epam.training.soap.fault.IncorrectDataFault;
import by.epam.training.soap.model.RequestAverage;
import by.epam.training.soap.model.ResponseAverage;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * SOAP service class.
 */
@WebService
public interface AverageService {

    /**
     * Return average for list of doubles.
     *
     * @param request request with list.
     * @return average value.
     * @throws IncorrectDataFault when sum of doubles less than 0.
     */
    @WebMethod
    public ResponseAverage getAverage(@WebParam(name = "RequestAverage") RequestAverage request)
            throws IncorrectDataFault;

}
