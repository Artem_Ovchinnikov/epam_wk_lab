package by.epam.training.soap.service;

import by.epam.training.soap.client.StatisticServiceClient;
import by.epam.training.soap.fault.IncorrectDataFault;
import by.epam.training.soap.fault.IncorrectDataFaultBean;
import by.epam.training.soap.model.RequestAverage;
import by.epam.training.soap.model.ResponseAverage;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Implementation for SOAP service.
 */
public class AverageServiceImpl implements AverageService {

    @Autowired
    private StatisticServiceClient client;

    @Override
    public ResponseAverage getAverage(RequestAverage request) throws IncorrectDataFault {
        List<Double> results = client.getSumAndAverage(request.getDoubles());
        Double sum = results.get(0);
        Double avg = results.get(1);
        if (sum < 0) {
            throw new IncorrectDataFault(new IncorrectDataFaultBean("Sum of elements must be great than 0!"));
        }
        ResponseAverage response = new ResponseAverage();
        response.setAverage(avg);
        return response;
    }
}
