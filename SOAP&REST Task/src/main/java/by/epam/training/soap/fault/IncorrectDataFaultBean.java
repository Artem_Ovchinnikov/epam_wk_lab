package by.epam.training.soap.fault;

/**
 * Bean for storing information about error.
 */
public class IncorrectDataFaultBean {

    private String message;

    public IncorrectDataFaultBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
