package by.epam.training.soap.client;

import net.webservicex.ArrayOfDouble;
import net.webservicex.Statistics;
import net.webservicex.StatisticsSoap;

import javax.xml.ws.Holder;
import java.util.ArrayList;
import java.util.List;

/**
 * Client for working with Statistics service.
 */
public class StatisticServiceClient {

    private Statistics service;
    private StatisticsSoap server;

    public StatisticServiceClient() {
        service = new Statistics();
        server = service.getStatisticsSoap();
    }

    /**
     * Send request to Statistics service and return sum and
     * average for list of double.
     *
     * @param arrayOfDouble list of doubles.
     * @return list of results. Sum of elements on position '0' in list,
     * average on position '1'.
     */
    public List<Double> getSumAndAverage(final ArrayOfDouble arrayOfDouble) {
        Holder<Double> sum = new Holder<>();
        Holder<Double> average = new Holder<>();
        server.getStatistics(arrayOfDouble, sum, average, new Holder<>(), new Holder<>(),
                new Holder<>());
        List<Double> results = new ArrayList<>();
        results.add(sum.value);
        results.add(average.value);
        return results;
    }

}
