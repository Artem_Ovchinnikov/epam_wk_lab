package by.epam.training.soap.model;

import net.webservicex.ArrayOfDouble;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * RequestAverage model.
 */
@XmlRootElement(name="GetAverageRequest")
public class RequestAverage {

    private ArrayOfDouble doubles;

    public RequestAverage() {
    }

    public ArrayOfDouble getDoubles() {
        return doubles;
    }

    public void setDoubles(ArrayOfDouble doubles) {
        this.doubles = doubles;
    }

}
