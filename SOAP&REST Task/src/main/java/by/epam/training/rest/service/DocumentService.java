package by.epam.training.rest.service;

import by.epam.training.rest.model.Chapter;
import by.epam.training.rest.model.Document;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Service class.
 */
@Produces("application/json")
@Consumes("application/xml")
public interface DocumentService {

    /**
     * Tries to find object in the repository.
     *
     * @param documentId ID for searching in repository.
     * @return object from repository or 404 code.
     */
    @GET
    @Path("/documents/{documentId}")
    public Response getDocument(@PathParam("documentId") long documentId);

    /**
     * Save new object in repository.
     *
     * @param document new object for storing.
     * @return new object.
     */
    @POST
    @Path("/documents")
    public Response addDocument(Document document);

    /**
     * Save new object in repository.
     *
     * @param document new object for storing.
     * @return new object.
     */
    @PUT
    @Path("/documents")
    public Response createDocument(Document document);

    /**
     * Update old object by ID or create new, if object wasn't found.
     *
     * @param documentId object ID in repository.
     * @param document   new version of object.
     * @return object, if he was created, or 204 code.
     */
    @PUT
    @Path("/documents/{documentId}")
    public Response updateDocument(@PathParam("documentId") long documentId, Document document);

    /**
     * Update chapter in document by chapter ID.
     *
     * @param documentId document ID.
     * @param chapterId  chapter ID.
     * @param chapter    new chapter version.
     * @return if document wasn't found - 404 code,
     * if chapter wasn't found - 201 code, else 204 code.
     */
    @PUT
    @Path("/documents/{documentId}/chapters/{chapterId}")
    public Response updateChapter(@PathParam("documentId") long documentId, @PathParam("chapterId") long chapterId,
                                  Chapter chapter);

    /**
     * Removes chapter in document by chapter ID.
     *
     * @param documentId document ID.
     * @param chapterId  chapter ID.
     * @return if chapter wasn't found in document - 404 code,
     * if chapter was last in document - 403 code, else 204 code.
     */
    @DELETE
    @Path("/documents/{documentId}/chapters/{chapterId}")
    public Response deleteChapter(@PathParam("documentId") long documentId, @PathParam("chapterId") long chapterId);

    /**
     * Removes object from repository.
     *
     * @param documentId object ID for removing from repository.
     * @return if document wasn't found - 404 code else 204 code.
     */
    @DELETE
    @Path("/documents/{documentId}")
    public Response deleteDocument(@PathParam("documentId") long documentId);

}
