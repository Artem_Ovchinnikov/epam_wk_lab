package by.epam.training.rest.repository;

import by.epam.training.rest.model.Document;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of DocumentRepository.
 */
public class DocumentRepositoryImpl implements DocumentRepository {

    private Map<Long, Document> storage = new HashMap<>();

    @Override
    public void add(Document document) {
        storage.put(document.getId(), document);
    }

    @Override
    public Document get(long documentId) {
        return storage.get(documentId);
    }

    @Override
    public void remove(long documentId) {
        storage.remove(documentId);
    }

    @Override
    public void update(long documentId, Document document) {
        storage.put(documentId, document);
    }

}
