package by.epam.training.rest.service;

import by.epam.training.rest.model.Chapter;
import by.epam.training.rest.model.Document;
import by.epam.training.rest.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Implementation of DocumentService.
 */
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository documentsRepository;

    @Override
    public Response getDocument(long documentId) {
        Document document = documentsRepository.get(documentId);
        if (document == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(document).build();
    }

    @Override
    public Response addDocument(Document document) {
        documentsRepository.add(document);
        return Response.status(Response.Status.CREATED).entity(document).build();
    }

    @Override
    public Response createDocument(Document document) {
        documentsRepository.add(document);
        return Response.status(Response.Status.CREATED).entity(document).build();
    }

    @Override
    public Response updateDocument(long documentId, Document document) {
        Document currentDocument = documentsRepository.get(documentId);
        if (currentDocument == null) {
            documentsRepository.add(document);
            return Response.status(Response.Status.CREATED).entity(document).build();
        }
        if (currentDocument.equals(document)) {
            return Response.notModified().build();
        }
        documentsRepository.update(documentId, document);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response updateChapter(long documentId, long chapterId, Chapter chapter) {
        Document document = documentsRepository.get(documentId);
        if (document == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        boolean isChapterWasFound = updateChapterInDocument(document, chapterId, chapter);
        if (!isChapterWasFound) {
            document.getChapters().add(chapter);
            return Response.status(Response.Status.CREATED).entity(document).build();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response deleteChapter(long documentId, long chapterId) {
        Document document = documentsRepository.get(documentId);
        if (document == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        boolean isChapterWasFound = deleteChapterInDocument(document, chapterId);
        if (!isChapterWasFound) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (document.getChapters().isEmpty()) {
            return Response.status(Response.Status.FORBIDDEN).entity("All chapters was deleted.").build();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response deleteDocument(long documentId) {
        Document document = documentsRepository.get(documentId);
        if (document == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        documentsRepository.remove(documentId);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    private boolean updateChapterInDocument(Document document, long chapterId, Chapter newChapter) {
        boolean isChapterWasUpdated = false;
        List<Chapter> chapters = document.getChapters();
        for (int i = 0; i < chapters.size(); i++) {
            Chapter chapter = chapters.get(i);
            if (chapter.getId() == chapterId) {
                chapters.remove(i);
                chapters.add(newChapter);
                isChapterWasUpdated = true;
            }
        }
        return isChapterWasUpdated;
    }

    private boolean deleteChapterInDocument(Document document, long chapterId) {
        boolean isChapterWasDeleted = false;
        List<Chapter> chapters = document.getChapters();
        for (int i = 0; i < chapters.size(); i++) {
            Chapter chapter = chapters.get(i);
            if (chapter.getId() == chapterId) {
                chapters.remove(i);
                isChapterWasDeleted = true;
            }
        }
        return isChapterWasDeleted;
    }

}
