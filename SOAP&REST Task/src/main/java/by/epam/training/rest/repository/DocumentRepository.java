package by.epam.training.rest.repository;

import by.epam.training.rest.model.Document;

/**
 * Repository for storing documents.
 */
public interface DocumentRepository {

    /**
     * Add new document in storage. ID for accessing the document is taken from the 'id' field.
     *
     * @param document document for storing.
     */
    public void add(Document document);

    /**
     * Returns an object by ID.
     *
     * @param documentId object ID for search.
     * @return object from storage or null, if object wasn't found.
     */
    public Document get(long documentId);

    /**
     * Removes an object from storage.
     *
     * @param documentId object ID for removing.
     */
    public void remove(long documentId);

    /**
     * Update the object in storage.
     *
     * @param documentId object ID in storage.
     * @param document   new version of the object.
     */
    public void update(long documentId, Document document);

}
